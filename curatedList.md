Books
-----
[Natural Language Processing](nlpwp.org/book)  
[Theoretical Minimum](http://theoreticalminimum.com/)  
[Nature of Code](http://natureofcode.com/book/)  
[Book of Bad Arguments](https://bookofbadarguments.com/?view=flipbook)  
[Distributed System](http://book.mixu.net/distsys/single-page.html)  
[List of free programming books](https://github.com/vhf/free-programming-books/blob/master/free-programming-books.md)  
[Learn SQL the hard way](http://sql.learncodethehardway.org/book/)  
[Graphics Programming Black Book](http://orangetide.com/graphics_programming_black_book/html/)  
[it-ebooks](http://it-ebooks.info/)  
[Python tips](http://book.pythontips.com/en/latest/)  
[Machine Learning] (https://github.com/rasbt/pattern_classification/blob/master/resources/machine_learning_ebooks.md)  
[Deep learning] (http://www.deeplearningbook.org/)  
[Artificial Intelligence] (http://artint.info/html/ArtInt.html)  
[Compiler Construction] (https://www.inf.ethz.ch/personal/wirth/CompilerConstruction/index.html)  
[Deep learning book](http://www.deeplearningbook.org/)  
[Linear Algebra](http://joshua.smcvt.edu/linearalgebra/)  
http://neuralnetworksanddeeplearning.com/  
[Zero MQ](http://zguide.zeromq.org/page:all)  
[CUDA](https://devblogs.nvidia.com/parallelforall/even-easier-introduction-cuda/)  
[Network programming in GO](https://jannewmarch.gitbooks.io/network-programming-with-go-golang-/content/)  
https://tuhdo.github.io/os01/  


Language
--------
[Go Lang](https://goo.gl/DE5QsL)  
[Erlang](http://spawnedshelter.com/)  
[Erlang](https://www.learnenough.com/command-line-tutorial)  


Journals/Papers
---------------
[ACCU Overload Journals](http://accu.org/index.php/journals/c78/)  
[Papers we love](http://paperswelove.org/)  
[Morning Paper: A Paper everyday](http://blog.acolyer.org/)  
[How to read a Technical Paper](http://www.cs.jhu.edu/~jason/advice/how-to-read-a-paper.html)  
[Paxos Made Simple] (http://research.microsoft.com/en-us/um/people/lamport/pubs/paxos-simple.pdf)  
[Paxos Made Live](http://static.googleusercontent.com/media/research.google.com/en//archive/paxos_made_live.pdf)  
[Paxos](https://www.quora.com/Distributed-Systems/What-is-a-simple-explanation-of-the-Paxos-algorithm)  
[Raft - An Understandable Consensus Algorithm](https://ramcloud.stanford.edu/raft.pdf)  
[Eric Demaine papers] (http://erikdemaine.org/papers/)  

Tech Blogs
----------
http://www.isthe.com/chongo/tech/comp/c/expert.html  
http://members.tip.net.au/~dbell/  
http://www.ccs.neu.edu/home/matthias/HtDP2e/index.html  
http://www.avc.com/  
http://venturebeat.com/  
http://allthingsd.com/  
http://www.technologyreview.com/  
http://arstechnica.com/  
http://archinect.com/  
http://core77.com/  
http://www.contemporist.com/  
http://www.reddit.com/r/ArtisanVideos/  
http://wireframes.linowski.ca/  
http://sidebar.io/  
http://hckrnews.com/  
http://gurneyjourney.blogspot.co.uk/  
http://www.economist.com/  
http://thedailywtf.com/  
http://zenhabits.net/  
http://dish.andrewsullivan.com/  
http://radar.oreilly.com/  
http://highscalability.com/  
http://www.dzone.com/links/index.html  
http://news.ycombinator.com/newest  
http://www.wired.com/  
http://www.theverge.com/  
http://skimfeed.com/  
http://hn.premii.com/#/article/6180467  
http://courses.engr.illinois.edu/cs425/fa2011/lectures.html  
https://wiki.engr.illinois.edu/display/cs598rco/Home  
https://sites.google.com/site/mriap2008/  
http://www.stanford.edu/class/cs106l/index.html  
http://users.ece.utexas.edu/~garg/jbk.html  
http://javapapers.com/  
http://www.stanford.edu/class/cs108/  
http://courses.engr.illinois.edu/cs525/sp2011/sched.htm  
http://learnpythonthehardway.org/  
http://googcloudlabs.appspot.com/  
http://algs4.cs.princeton.edu/home/  
https://gitlab.com/doctorj/interview-questions  
http://danluu.com/programming-blogs/  
http://belkadan.com/blog/2015/11/Recommendations/  
http://the-paper-trail.org/blog/distributed-systems-theory-for-the-distributed-systems-engineer/  
http://concurrencyfreaks.blogspot.com/  
http://www.catonmat.net/blog/learning-python-design-patterns-through-video-lectures/  
http://muratbuffalo.blogspot.com/2016/11/my-distributed-systems-seminars-reading.html  
https://jeremykun.com/2012/08/04/machine-learning-introduction/  
http://hyperpolyglot.org/  
https://learnxinyminutes.com/  
http://canonical.org/~kragen/memory-models/  
http://canonical.org/~kragen/  
https://github.com/michelpereira/gamesofcoding  
http://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html  
http://www.lecloud.net/post/7295452622/scalability-for-dummies-part-1-clones  
https://github.com/kamranahmedse/design-patterns-for-humans/blob/master/README.md  
https://github.com/terryum/awesome-deep-learning-papers  
https://github.com/eriklindernoren/ML-From-Scratch  
https://cloud.google.com/apis/design/  
http://www-cs-students.stanford.edu/~amitp/gameprog.html  
https://software.intel.com/en-us/articles/the-ultimate-question-of-programming-refactoring-and-everything  
http://course.fast.ai/lessons/lessons.html  
https://teachyourselfcs.com/  
https://barrgroup.com/blog  


Podcasts
--------
http://www.se-radio.net/  
http://androidbackstage.blogspot.com/  
http://thisdeveloperslife.com/  
http://embedded.fm/  
https://news.ycombinator.com/item?id=13254983  

Articles
--------
http://www.toptal.com/data-science/algorithmic-trading-a-practical-tale-for-engineers  
[Cassandra](http://www.datastax.com/documentation/articles/cassandra/cassandrathenandnow.html)  
http://alexkrupp.typepad.com/sensemaking/2013/11/2012-my-year-of-code.html  
[Readings in Distributed Systems](http://christophermeiklejohn.com/distributed/systems/2013/07/12/readings-in-distributed-systems.html)  
[Distributed systems theory](http://the-paper-trail.org/blog/distributed-systems-theory-for-the-distributed-systems-engineer/)  
[Readings in Databases](https://github.com/rxin/db-readings)  
[What CS engineer should know](http://matt.might.net/articles/what-cs-majors-should-know/)  
[How to write unmaintainable code](https://www.doc.ic.ac.uk/~susan/475/unmain.html)  
https://blog.growth.supply/60-youtube-channels-that-will-make-you-smarter-44d8315c2548  
https://github.com/btrask/stronglink/blob/master/SUBSTANCE.md  
http://c.learncodethehardway.org/book/  
http://learnpythonthehardway.org/book/preface.html  
http://c2.com/cgi/wiki?ReallyValuablePages  
http://coding-geek.com/how-databases-work/?utm_source=hackernewsletter&utm_medium=email&utm_term=fav    
https://github.com/rushter/data-science-blogs  
https://github.com/kilimchoi/engineering-blogs  
http://www.catonmat.net/blog/top-100-books-part-three/  
https://github.com/0xAX/linux-insides/blob/master/interrupts/interrupts-1.md  
http://0xax.gitbooks.io/linux-insides/content/  
http://dfir.org/?q=node/8/&utm_source=hackernewsletter&utm_medium=email&utm_term=books  
http://research.google.com/pubs/DistributedSystemsandParallelComputing.html  
http://research.google.com/pubs/papers.html  
http://grid.cs.gsu.edu/~cscnxx/  
https://research.facebook.com/publications  
http://highscalability.com/blog/2011/9/14/big-list-of-scalabilty-conferences.html  
http://research.microsoft.com/en-us/um/people/lamport/pubs/pubs.html  
https://dl.dropboxusercontent.com/u/315/articles/index.html  
https://www.reddit.com/r/vrd  
http://jeffhuang.com/best_paper_awards.html  
http://arxiv.org/  
http://blogspot.fluidnewmedia.com/link-goodness/  
https://sites.google.com/site/mathprogrammingbooks/  
http://sam-koblenski.blogspot.de/2013/06/the-6-best-blogs-for-software-developer.html  
http://www.catonmat.net/blog/low-level-bit-hacks-you-absolutely-must-know/  
http://phrack.org/issues/69/1.html  
http://theaigames.com/  
http://overapi.com/#more  
http://aosabook.org/en/index.html  
[Reading list](http://www.squeakland.org/resources/books/readingList.jsp)  
https://github.com/vic317yeh/One-Click-to-Be-Pro  
http://www.stanford.edu/class/cs97si/  
http://www.brokenthorn.com/Resources/OSDevIndex.html  
https://80000hours.org/career-guide/how-to-get-a-job/  
http://matt.might.net/articles/best-programming-languages/  
https://cheatdeath.github.io/research-bittorrent-doc/  
https://github.com/vhf/free-programming-books  
https://graphics.stanford.edu/~seander/bithacks.html  
https://www.elastic.co/guide/en/elasticsearch/guide/current/getting-started.html  
https://github.com/vic317yeh/One-Click-to-Be-Pro  
http://javapapers.com/category/design-patterns/  
http://c9x.me/compile/bib/  
http://joearms.github.io/2013/11/21/My-favorite-erlang-program.html  
https://culurciello.github.io/tech/2016/06/04/nets.html  
https://www.quantstart.com/articles/How-to-Learn-Advanced-Mathematics-Without-Heading-to-University-Part-3  
http://courses.csail.mit.edu/iap/interview/materials.php  
http://www.developersbook.com/corejava/interview-questions/corejava-interview-questions-faqs.php  
http://stackoverflow.com/questions/58354/algorithm-data-structure-design-interview-questions  
https://www.linkedin.com/pulse/20140425202654-10752460-what-to-do-and-not-do-if-i-interview-you  
https://www.linkedin.com/pulse/interviews-exams-jacob-kessler  
https://www.linkedin.com/pulse/20140205163949-28614372-getting-that-job-at-linkedin  
https://www.linkedin.com/pulse/design-interview-from-interviewers-perspective-joey-addona  
https://www.linkedin.com/pulse/success-coding-interview-joey-addona  
https://developers.google.com/edu/  
http://norvig.com/spell-correct.html  
http://jvns.ca/zines/  
https://github.com/datasciencemasters/go  
http://www.downes.ca/post/38526  
https://medium.com/learning-new-stuff/machine-learning-in-a-year-cdb0b0ebd29c#.muu6r1w51  
http://learning.cis.upenn.edu/cis520_fall2009/index.php?n=Lectures.Lectures  
https://see.stanford.edu/Course/EE261  
https://github.com/kelseyhightower/kubernetes-the-hard-way  
http://ai-on.org/  
http://alumni.media.mit.edu/~jorkin/aibooks.html  
https://80000hours.org/career-guide/job-satisfaction/  
https://cloud.google.com/blog/big-data/2017/01/learn-tensorflow-and-deep-learning-without-a-phd  
https://www.youtube.com/playlist?list=PLrAXtmErZgOeiKm4sgNOknGvNjby9efdf  
https://docs.google.com/document/d/1MBpwNLl3AgsKi4Yh4PZ2X30PYy4gV9xK2xgqXKevaVQ/edit  
https://github.com/donnemartin/system-design-primer  

Courses
-------
[Machine Learning] (http://alex.smola.org/teaching/cmu2013-10-701x/index.html)  
[Course List](https://github.com/prakhar1989/awesome-courses/blob/master/README.md)  
[Data Science](https://github.com/justmarkham/DAT8)  
[Stanford Deep learning](https://www.youtube.com/watch?v=kZteabVD8sU&list=PLmImxx8Char9Ig0ZHSyTqGsdhb9weEGam)  
[Deep Learning for Natural Language Processing](https://web.archive.org/web/20160314075834/http://cs224d.stanford.edu/syllabus.html)  
http://ai.berkeley.edu/lecture_videos.html  
http://ai.berkeley.edu/course_schedule.html  
http://online.cambridgecoding.com/courses  
http://courses.cms.caltech.edu/cs179/  
https://openai.com/blog/concrete-ai-safety-problems/  
http://blog.fogus.me/2009/05/29/pet-projects/  
https://github.com/karan/Projects  
https://deepmind.com/blog  
http://online.cambridgecoding.com/notebooks/eWReNYcAfB/implementing-your-own-recommender-systems-in-python-2?utm_source=hackernewsletter&utm_medium=email&utm_term=code  
http://ofir.io/How-to-Start-Learning-Deep-Learning/  
https://courses.engr.illinois.edu/cs425/fa2011/lectures.html  
https://sites.google.com/site/mriap2008/  
http://15721.courses.cs.cmu.edu/spring2016/schedule.html  
http://web.stanford.edu/class/cs276/  
https://gym.openai.com/  
[Data Science]https://github.com/datasciencemasters/go  
https://github.com/kelseyhightower/kubernetes-the-hard-way  
https://see.stanford.edu/Course/EE261
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/lecture-videos/  
https://medium.freecodecamp.com/ten-rules-for-negotiating-a-job-offer-ee17cccbdab6#.x403c6c8v  
[Kernel](https://www.ops-class.org/)  
[Projects](http://nifty.stanford.edu/)  
[MIT Advanced Data Structures](http://courses.csail.mit.edu/6.851/)  
https://github.com/Developer-Y/cs-video-courses/blob/master/README.md  
[AI Course](http://course.fast.ai/)  
[Stanford DeepLearning Tutorial](http://deeplearning.stanford.edu/tutorial/)  

MIT OCW  
[introduction-to-algorithms-fall-2011](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/)  
[introduction-to-algorithms-spring-2008](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-spring-2008/syllabus/)  
[artificial-intelligence-fall-2010](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/calendar/)  
[artificial-intelligence-spring-2005](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-spring-2005/syllabus/)  
[design-and-analysis-of-algorithms-spring-2012](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2012/calendar/)  
[introduction-to-algorithms-sma-5503-fall-2005](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-introduction-to-algorithms-sma-5503-fall-2005/)  
[practical-programming-in-c-january-iap-2010](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-087-practical-programming-in-c-january-iap-2010/calendar/)  
[introduction-to-c-memory-management-and-c-object-oriented-programming-january-iap-2010](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-088-introduction-to-c-memory-management-and-c-object-oriented-programming-january-iap-2010/lecture-notes/)  
[multicore-programming-primer-january-iap-2007](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-189-multicore-programming-primer-january-iap-2007/syllabus/)  
[networks-fall-2009](http://ocw.mit.edu/courses/economics/14-15j-networks-fall-2009/calendar/)  
[parallel-computing-fall-2011](http://ocw.mit.edu/courses/mathematics/18-337j-parallel-computing-fall-2011/related-resources/)  
[machine-vision-fall-2004](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-801-machine-vision-fall-2004/syllabus/)  
[database-systems-fall-2010](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-830-database-systems-fall-2010/Syllabus/)  
[automatic-speech-recognition-spring-2003](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-345-automatic-speech-recognition-spring-2003/lecture-notes/)  
[computer-system-architecture-fall-2005](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-823-computer-system-architecture-fall-2005/readings/)  
[distributed-computer-systems-engineering-spring-2006](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-824-distributed-computer-systems-engineering-spring-2006/lecture-notes/)  
[techniques-in-artificial-intelligence-sma-5504-fall-2002](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-825-techniques-in-artificial-intelligence-sma-5504-fall-2002/syllabus/)  
[multithreaded-parallelism-languages-and-compilers-fall-2002](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-827-multithreaded-parallelism-languages-and-compilers-fall-2002/lecture-notes/)  
[advanced-data-structures-spring-2012](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-851-advanced-data-structures-spring-2012/calendar-and-notes/)  
[advanced-algorithms-fall-2008](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-854j-advanced-algorithms-fall-2008/readings/)  
[network-and-computer-security-spring-2014](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-857-network-and-computer-security-spring-2014/Syllabus/)  
[computer-systems-security-fall-2014](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-858-computer-systems-security-fall-2014/final-project/)  
[machine-learning-fall-2006-MIT-OCW](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-867-machine-learning-fall-2006/download-course-materials/)  
[AI Berkeley] (http://ai.berkeley.edu/project_overview.html)  
http://ai.berkeley.edu/tutorial.html  
http://ai.berkeley.edu/lecture_videos.html  
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/lecture-videos/  

Workout
-------
[The scientific 7 minute workout](http://well.blogs.nytimes.com/2013/05/09/the-scientific-7-minute-workout/)
[Build navy seal strength](http://www.climbing.com/skill/build-navy-seal-strength/)

Tutorials
---------
[Vim](https://danielmiessler.com/study/vim/)
https://docs.oracle.com/javase/tutorial/getStarted/end.html)  
https://docs.oracle.com/javase/tutorial/extra/generics/index.html)  
https://docs.oracle.com/javase/tutorial/reallybigindex.html)  
http://learnyousomeerlang.com/content)  
http://www.hboehm.info/gc/)  
http://www.slideshare.net/olvemaudal/deep-c)  
http://codecapsule.com/2014/02/12/coding-for-ssds-part-1-introduction-and-table-of-contents/)  
[Dive into Machine Learning](https://github.com/hangtwenty/dive-into-machine-learning)  
[Java 8](https://github.com/winterbe/java8-tutorial)  
[Neural Networks](http://lumiverse.io/series/neural-networks-demystified)  
[Topcoder](https://www.topcoder.com/community/data-science/data-science-tutorials/)
https://apps.topcoder.com/wiki/display/tc/Algorithm+Problem+Set+Analysis


OpenSource projects
-------------------
[Mozilla](https://developer.mozilla.org/en-US/Learn)  
[Firefox](http://codefirefox.com/cheatsheet)  
[Mesos](https://mesos.apache.org/documentation/latest/)  
[CoreOS](https://coreos.com/)  
[Zulip](https://www.zulip.org/contribute.html)  
[Mesosphere](https://mesosphere.com/)  
[Lucene Solr](https://lucene.apache.org/solr/)  
[Elastic](https://www.elastic.co/)  
https://github.com/elastic/elasticsearch
[Better Java](https://github.com/cxxr/better-java)  
siem  
passport.js    
[postgresql](http://www.postgresql.org/)  
[cassandra](https://cassandra.apache.org/)  
[hadoop](https://hadoop.apache.org/)  
[golang](https://golang.org/)  
[spark](https://spark.apache.org/)  
[cordova](https://cordova.apache.org/)  
[couchdb](https://couchdb.apache.org/)  
[flex](https://flex.apache.org/)  
[geronimo](http://geronimo.apache.org/)  
[apache](https://httpd.apache.org/)  
[lucene](https://lucene.apache.org/core/)  
[maven](https://maven.apache.org/)  
[openoffice](https://www.openoffice.org/)  
[pig](https://pig.apache.org/)  
[subversion](https://subversion.apache.org/)  
[openstack](https://www.openstack.org/)  
[docker](https://www.docker.com/)  
[mapsme](https://github.com/mapsme/omim)  
[Tensorflow](http://tensorflow.org/)  
[IBm SystemML](https://github.com/SparkTC/systemml)  
[Alchemy Open Sourec AI](http://alchemy.cs.washington.edu/)  
[Redis](http://redis.io/)  
[P4](http://p4.org/)  
[OpenSwitch](http://openswitch.net/)  
[OpenSnapRoute](https://opensnaproute.github.io/docs/architecture.html)  
[Apache Thrift](https://thrift.apache.org/)  
[Redis](https://redis.io/)  
[NanoMSG](http://nanomsg.org/)  

Security
--------
http://www.troyhunt.com/2015/09/troys-ultimate-list-of-security-links.html  
http://www.securityweek.com/six-essential-free-tools-security-teams  
https://engineering.opendns.com/2015/03/16/security-ninjas-an-open-source-application-security-training-program/  
https://www.exploit-db.com/google-hacking-database/  
http://beefproject.com/  
https://twitter.com/DarkReading  
https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/  
http://www.fuzzysecurity.com/tutorials/16.html  
https://www.offensive-security.com/metasploit-unleashed/  
https://www.exploit-db.com/  
http://www.sans.org/security-resources/  
http://hackerstorm.co.uk/  
http://www.eccouncil.org/  
http://www.securitymagazine.com/  
http://www.securityfocus.com/  
http://www.windowsecurity.com/blogs/  
http://www.hackerjournals.com/  
http://www.zdnet.com/topic/security/  
http://capec.mitre.org/index.html  
https://www.offensive-security.com/metasploit-unleashed/requirements/  




Programming Challenges
----------------------
[IBM Ponder This - Monthly puzzle](https://www.research.ibm.com/haifa/ponderthis/index.shtml)  
[Art of Problem Solving](http://www.artofproblemsolving.com/alcumus)  
[Code Kata](http://codekata.com/)  
[Less Than Dot](http://forum.lessthandot.com/viewforum.php?f=102)  
[Daily WTF](http://thedailywtf.com/series/bring-your-own-code)  
[Google CodeJam](https://code.google.com/codejam/contests.html)  
[TopCoder](http://www.topcoder.com/)  
[Hacker challenge](http://www.hacker.org/challenge/about.php)  
[CodeChef](https://www.codechef.com/)  
[CodeWars](http://www.codewars.com/)  
[Rosalind](http://rosalind.info/problems/locations/)  
[Coding Game](https://www.codingame.com/start)  
[SPOJ](http://www.spoj.com/problems/classical/)  
[Cyber Dojo](http://www.cyber-dojo.org/)  
[leetcode](https://leetcode.com/)   
[Smash the Stack](http://smashthestack.org/)  
[Bright Shadows](http://bright-shadows.net/)  
[C Questions](http://www.gowrikumar.com/c/index.php)  
[Codegolf stackexchange](http://codegolf.stackexchange.com/)  
[Programming praxis](http://programmingpraxis.com/)  
[coding bat](http://codingbat.com/)  
[CodeCondo](http://codecondo.com/coding-challenges/)  
[Reddit Daily Programmer](http://www.reddit.com/r/dailyprogrammer)  
[Codility](https://codility.com/programmers/)  
[Reddit Learn Programming](http://www.reddit.com/r/learnprogramming/wiki/index)  
https://www.kaggle.com/  
http://www0.us.ioccc.org/index.html  
http://tutorials.jenkov.com/java-concurrency/index.html  
http://www.azspcs.net/  
http://www.pythonchallenge.com/  
https://www.hackerrank.com/work/?utm_source=recruit2&utm_campaign=rebranding  
http://everything2.com/title/hard%2520interview%2520questions  
https://www.ocf.berkeley.edu/~wwu/riddles/cs.shtml  
http://www.mindcipher.com/  
codingbat.com  
https://github.com/gdb/stripe-prg/wiki/Papers  
https://codegolf.stackexchange.com/questions  
https://icpc.baylor.edu/worldfinals/problems  
https://archive.ics.uci.edu/ml/  
http://www.dataschool.io/  
http://radimrehurek.com/data_science_python/  
https://www.hackerrank.com/feed  
http://www.crazyforcode.com/  
http://www.programmerinterview.com/  
http://howtodoinjava.com/  
http://codercareer.blogspot.com/  
https://leetcode.com/problemset/algorithms/  
http://www.codeforces.com/  
http://codingforinterviews.com/practice  
https://projecteuler.net/archives  
https://www.interviewcake.com/  
https://adventofcode.com/  
https://www.reddit.com/r/dailyprogrammer/  
http://www.techiedelight.com/list-of-problems/  
http://www.datasciencecentral.com/profiles/blogs/how-to-become-a-data-scientist-for-free  
http://www.techiedelight.com/graphs-interview-questions/  